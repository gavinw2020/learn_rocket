# 测试请求 

```rust
#[macro_use] extern crate rocket;
use rocket::serde::{Serialize, json::Json};

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
struct Location {
    lat: String,
    lng: String,
}

#[get("/?<lat>&<lng>")]
fn location(lat: &str, lng: &str) -> Json<Location> {
    Json(Location {
        lat: 111.1111.to_string(),
        lng: 222.2222.to_string(),
    })
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![location])
}
```

## request
```url
http://localhost/?lat=1&lng=2
```
## response
```json
{"lat":"111.1111","lng":"222.2222"}
```


# 连接MySQL

## Cargo.toml
```
[package]
name = "rust_mysql_demo"
version = "0.1.0"
edition = "2021"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
mysql = { version = "24.0.0"  }
rocket =  { version = "0.5.0", features = ["json"] }
rocket_db_pools = { version = "0.1.0", features = ["sqlx_mysql"] }
 
sqlx =  { version = "0.7.3"  }
serde = { version = "1.0.193", features = ["derive"] }
```

## .env
```
DATABASE_URL=mysql://gavin:gavin@127.0.0.1/db_test
```

## Rocket.toml
```
[default]
port = 8080

[default.databases.mysql_db]
url = "mysql://gavin:gavin@127.0.0.1/db_test"
```

## main.rs
```rust
#[macro_use] extern crate rocket;

use rocket_db_pools::{Database, Connection};
use rocket_db_pools::sqlx::{self, Row, query}; 

#[derive(Database)]
#[database("mysql_db")]
struct Logs(sqlx::MySqlPool);


#[get("/<id>")]
async fn read(mut db: Connection<Logs>, id: i64) -> Option<String> {
    sqlx::query("SELECT f_username FROM tb_user WHERE f_id=?").bind(id)
        .fetch_one(&mut **db).await
        .and_then(|r| Ok(r.try_get(0)?))
        .ok()
}

#[launch]
fn rocket() -> _ {
   rocket::build().attach(Logs::init()).mount("/", routes![read])
}
```

## request
```url
http://localhost/1
```
## response
返回 f_id=1 的 [f_username] 的内容

# 返回mysql的json

## Rocket.toml
```
[default]
port = 8080

[default.databases.mysql_db]
url = "mysql://gavin:gavin@127.0.0.1/db_test"
# 这里都是默认值 可以不设置
min_connetcions = 64
max_connetcions = 1024
connect_timeout = 5
idle_timeout= 120
```
... 其他文件同上个例子

## main.rs
```rust
#[macro_use] extern crate rocket;

use rocket::serde::{Serialize, json::Json};

use rocket_db_pools::{Database, Connection};
use rocket_db_pools::sqlx::{self}; 

use futures::{ future::TryFutureExt, stream::TryStreamExt};

#[derive(Database)]
#[database("mysql_db")]
struct Logs(sqlx::MySqlPool);

type Result<T, E = rocket::response::Debug<sqlx::Error>> = std::result::Result<T, E>;

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
struct FUser {
    f_id:Option<i32>,
    f_username: Option<String>,
    f_realname: Option<String>,
    f_uuid:Option<String>
}

#[get("/<id>")]
async fn read(mut db: Connection<Logs>, id: i64) -> Option<Json<FUser>> {
    sqlx::query!("SELECT f_id,f_username,f_realname,f_uuid FROM tb_user WHERE f_id = ?",id)
        .fetch_one(&mut **db)
        .map_ok(|r| Json(FUser { f_id: Some(r.f_id), f_username: r.f_username, f_realname: r.f_realname, f_uuid: r.f_uuid }))
        .await 
        .ok()
}

#[get("/all")]
async fn read_all(mut db: Connection<Logs>) -> Result<Json<Vec<FUser>>> {
    let users = sqlx::query!("SELECT f_id,f_username,f_realname,f_uuid FROM tb_user")
        .fetch(&mut **db)
        .map_ok(|r| FUser{f_id: Some(r.f_id), f_username: r.f_username, f_realname: r.f_realname, f_uuid: r.f_uuid})
        .try_collect::<Vec<_>>()
        .await?;
        Ok(Json(users))
}

#[launch]
fn rocket() -> _ {
   rocket::build()
   .attach(Logs::init())
   .mount("/", routes![read,read_all]) 
}
```




# rust 函数前面的 match 的作用

在Rust中，match关键字用于模式匹配，它允许你根据不同的模式对值进行分支处理。当你在函数或方法中使用match时，你正在定义一个返回类型为Result的函数，并根据匹配的结果返回不同的Result变体。

这里有一个简单的示例来说明如何使用match：

```rust
fn calculate_result(value: i32) -> Result<i32, String> {
    match value {
        0 => Ok(0),
        1 => Ok(1),
        2 => Ok(2),
        _ => Err(format!("Invalid value: {}", value)),
    }
}

fn main() {
    let result = calculate_result(5);
    match result {
        Ok(value) => println!("Result: {}", value),
        Err(error) => println!("Error: {}", error),
    }
}
```

在上面的例子中，calculate_result函数接受一个i32类型的参数，并返回一个Result<i32, String>类型的值。根据输入的值，函数使用match进行模式匹配，并返回相应的Ok或Err变体。在main函数中，我们再次使用match来处理返回的Result值。

使用match可以使代码更加清晰和可读，并帮助你更好地处理可能出现的错误。