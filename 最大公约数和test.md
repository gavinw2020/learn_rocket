```rust
fn main() {
    println!("Hello, world!");
    println!("{}",gcd(14, 15));
}
// 公约数函数
fn gcd(mut a:u64,mut b:u64) -> u64{
    assert!(a!=0 && b!=0);
    while b!=0 {
        if b<a {
            let t=b;
            b=a;
            a=t;
        }
        b=b%a;
    }
    a
}
// 测试模块
#[test]
fn test_gcd(){
    assert_eq!(gcd(14, 15),1);
    assert_eq!(gcd(3*5*8,3*5),(3*5));
}
```