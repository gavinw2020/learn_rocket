#[macro_use]
extern crate rocket;

mod module;
mod routes;

use routes::*;

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index])
        // add api
        .mount("/", routes![get_article_list, get_article_by_id])
        .mount("/", routes![post_article, delete_article, put_article])
}
