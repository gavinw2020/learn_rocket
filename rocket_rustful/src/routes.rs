use rocket::serde::json::{serde_json::json, Json, Value};
use rocket::{delete, get, post, put};

use crate::module::Article;

#[get("/")]
pub fn index() -> &'static str {
    "Hello, world!"
}

#[get("/api")]
pub async fn get_article_list() -> Value {
    json!({"res": "Test Success!"})
}

#[get("/api/<in_id>")]
pub async fn get_article_by_id(in_id: usize) -> Option<Json<Article>> {
    Some(Json(Article {
        id: in_id,
        title: "a title".to_string(),
        author: "恐咖兵糖".to_string(),
        content: "一些内容".to_string(),
        created_at: "2022-02-14 ".to_string(),
    }))
}

#[post("/api", format = "json", data = "<article>")]
pub async fn post_article(article: Json<Article>) -> Value {
    json!({"res": "Post Success!","post": format!("{:?}", article) })
}

#[put("/api/<in_id>", format = "json", data = "<article>")]
pub async fn put_article(in_id: usize, article: Json<Article>) -> Value {
    json!({"res": "Put Success!","id": in_id,"put": format!("{:?}",article) })
}

#[delete("/api/<in_id>")]
pub async fn delete_article(in_id: usize) -> Value {
    json!({"res": "Delect Success","id": in_id })
}
