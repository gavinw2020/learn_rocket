use rocket::serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(crate = "rocket::serde")]
pub struct Article {
    pub id: usize,
    pub title: String,
    pub author: String,
    pub content: String,
    pub created_at: String,
}
