#[macro_use] extern crate rocket;

use rocket_db_pools::{Database, Connection};
use rocket_db_pools::sqlx::{self, Row, query}; 

#[derive(Database)]
#[database("mysql_db")]
struct Logs(sqlx::MySqlPool);


#[get("/<id>")]
async fn read(mut db: Connection<Logs>, id: i64) -> Option<String> {
    sqlx::query("SELECT f_username FROM tb_user WHERE f_id=?").bind(id)
        .fetch_one(&mut **db).await
        .and_then(|r| Ok(r.try_get(0)?))
        .ok()
}

#[launch]
fn rocket() -> _ {
   rocket::build().attach(Logs::init()).mount("/", routes![read])
}